<?php
//分账接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\LaunchLedger;

try {
    $trade_query = new LaunchLedger();
    $trade_query->mno = '399190910000387';
    //商户订单号
    $trade_query->uuid = '';
    //分账对应的原交易商户订单号
    $trade_query->ordNo = 'MT_TEST_' . date('YmdHis');
    //是否分账 00:不分账01：做分账
    $trade_query->ledgerAccountFlag = '01';
    //分账规则
    $rule_arr = [
        ["allotValue" => 0.01,"mno" =>399190618057330],
        ["allotValue" => 0.01,"mno" => 399190618057330]
    ];
    $trade_query->ledgerRule = json_encode($rule_arr);
    $client = new AopClient($config);


    $res = $client->execute($trade_query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

