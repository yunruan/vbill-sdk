<?php
//退款查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\RefundQuery;

try {
    $refund_query = new RefundQuery();
    $refund_query->mno = '399190618057330';
    //商户订单号
    $refund_query->ordNo = 'MT_TEST_' . date('YmdHis');

    $client = new AopClient($config);


    $res = $client->execute($refund_query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

