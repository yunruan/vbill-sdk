<?php
//商户设置

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\MerchantSetup;

try {
    $setup = new MerchantSetup();

    //商编
    $setup->mno = '399200529063975';

    //01 微信02 支付宝06 银联单笔小亍等亍 1000 07 银联单笔大亍 1000
    $qrcodeType = array("01", "02", "06", "07");
    //二维码费率（费率单位为%） json 形式传入修改费率必须完整传入
    $qrcoderate = array(5, 5, 5, 5);
    $qrcodeList = [];
    for ($i = 0; $i < count($qrcodeType); $i++) {
        $qrcodeList[] = [
            "rateType" => $qrcodeType[$i],
            "rate" => $qrcoderate[$i]
        ];
    }
    $setup->qrcodeList = $qrcodeList;

    //结算类型 03 T1 04 D1
    $setup->settleType = '03';
    //支持的支付渠道 01 支付宝 02 微信 03 银联二维码
    $setup->supportPayChannels = ['01','02'];
    //支持的交易类型 01 主扫 02 被扫 03 公众号/小程序/服务窗/银 联 JS 04 退货 05 APP
    $setup->supportTradeTypes = ["01","02","03","04", "05" ];

    $client = new AopClient($config);

    $res = $client->execute($setup);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

