<?php
//关单接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\Close;

try {
    $close = new Close();
    $close->mno = '399190618057330';
    //商户订单号
    $close->origOrderNo = 'MT_TEST_' . date('YmdHis');

    $client = new AopClient($config);


    $res = $client->execute($close);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

