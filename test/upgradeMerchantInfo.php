<?php
//小微商户补充资料升级接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\UpgradeMerchantInfo;

try {
    $upgrade = new UpgradeMerchantInfo();

    $upgrade->mno = '399200529063975';
    //资质类型 02 个体户 03
    $upgrade->haveLicenseNo = '03';
    //营业执照注册名称
    $upgrade->cprRegNmCn = '澧县xxxxx责任公司';
    //营业执照注册号
    $upgrade->registCode = '914xxxxxxRLX3';
    //是否三证合一 企业必传 00 是 01 否
    $upgrade->licenseMatch = '00';
    //营业执照照片
    $upgrade->licensePic = 'c57533d4412f441fb215b18f4f4f5d61';
    //法人/商户负责人身份证正面（人像面）
    $upgrade->legalPersonidPositivePic = 'c57533d4412f441fb215b18f4f4f5d61';
    //法人/商户负责人身份证正面（国徽面）
    $upgrade->legalPersonidOppositePic = 'c57533d4412f441fb215b18f4f4f5d61';
    //门头照片
    $upgrade->storePic = 'c57533d4412f441fb215b18f4f4f5d61';
    //真实商户内景图片
    $upgrade->insideScenePic = 'c57533d4412f441fb215b18f4f4f5d61';
    //开户许可证 对公结算必传
    $upgrade->openingAccountLicensePic = 'c57533d4412f441fb215b18f4f4f5d61';

    $client = new AopClient($config);


    $res = $client->execute($upgrade);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

