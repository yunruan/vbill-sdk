<?php
//商户信息修改

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\EditMerchantInfo;

try {
    $edit = new EditMerchantInfo();

    //商编
    $edit->mno = '399200529063975';
    //商户简称
    $edit->mecDisNm = '签购单的的名称';
    //商户联系手机号
    $edit->mblNo = '13512345678';
    //商户经营详细地址
    $edit->cprRegAddr = '西商xxxxxx无校验';
    //商户经营地址省编码
    $edit->regProvCd = '130000000000';
    //商户经营地址市编码
    $edit->regCityCd = '130700000000';
    //商户经营地址区编码
    $edit->regDistCd = '130728000000';
    //客服电话
    $edit->csTelNo = '13812345678';
    //结算账户名
    $edit->actNm = '澧县xxxxx责任公司';
    //结算账户类型 00 对公 01 对私
    $edit->actTyp = '00';
    //结算人证件号
    $edit->stmManIdNo = '4324xxxxxxX';
    //结算卡号
    $edit->actNo = '127913030410802';
    //开户银行
    $edit->depoBank = '102';
    //开户省份
    $edit->depoProvCd = '11';
    //开户城市
    $edit->depoCityCd = '1100';
    //开户支行联行行号
    $edit->lbnkNo = '787521000004';
    //开户支行名称
    $edit->lbnkNm = 'xxxxx支行';
    //营业执照照片
    $edit->licensePic = '61f60aa782f144318efe49831655dde0';
    //法人/商户负责人身份证正面（人像面）
    $edit->legalPersonidPositivePic = '61f60aa782f144318efe49831655dde0';
    //法人/商户负责人身份证正面（国徽面）
    $edit->legalPersonidOppositePic = '61f60aa782f144318efe49831655dde0';
    //门头照片
    $edit->storePic = '61f60aa782f144318efe49831655dde0';
    //真实商户内景图片
    $edit->insideScenePic = '61f60aa782f144318efe49831655dde0';
    //开户许可证 对公结算必传
    $edit->openingAccountLicensePic = '61f60aa782f144318efe49831655dde0';

    $client = new AopClient($config);


    $res = $client->execute($edit);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

