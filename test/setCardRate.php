<?php
//商户刷卡费率设置

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\SetCardRate;

try {
    $set_card_rate = new SetCardRate();
    $set_card_rate->mno = '399190910000387';

    //21贷记卡费率，不传默认0.60  22借记卡费率，不传默认0.55  23借记卡手续费封顶值，不传默认25  若传入，则对应三个类型必须同时传入
    $bankrate = array(0.60, 0.55, 25);
    //刷卡费率（费率单位为%，封顶值单位为元） json格式传入
    $bankType = array("21", "22", "23");
    $bankCardRates = [];
    for ($i = 0; $i < count($bankType); $i++) {
        $bankCardRates[] = [
            "rate" => $bankrate[$i],
            "type" => $bankType[$i]
        ];
    }
    $set_card_rate->bankCardRates = $bankCardRates;

    $client = new AopClient($config);


    $res = $client->execute($set_card_rate);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

