<?php
//商户入驻

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\Income;

try {
    $income = new Income();

    //商户简称
    $income->mecDisNm = '签购单的的名称';
    //商户联系手机号
    $income->mblNo = '13512345678';
    //经营类型 01 线下 02 线上 03 非盈利类 04 缴费类 05 保险类 06 私立院校
    $income->operationalType = '01';
    //资质类型 01 自然人 02 个体户 03 企业 线上商户必须是 03 企业类型
    $income->haveLicenseNo = '03';
    //商户类型 00 普通单店商户（非连锁商户）01 连锁总 02 连锁分 03 1+n 总 04 1+n 分
    $income->mecTypeFlag = '00';

    if(in_array($income->mecTypeFlag, array('02','04'))) {
        //所属总店商户编号  商户类型为02，04 时必填  商户类型是02连锁分店时，总店类型必须是01连锁总店  商户类型是04 1+n分店时，总店类型必须是03 1+n总店
        $income->parentMno = '399200529394890';
        //分店是否独立结算 00是 01否  不传默认00独立结算  商户类型是02（连锁分店）时必传此参数  其他情况传入无效，系统强制为00独立结算
        $income->independentModel = '00';
    }

    //01 微信02 支付宝06 银联单笔小亍等亍 1000 07 银联单笔大亍 1000
    $qrcodeType = array("01", "02", "06", "07");
    //二维码费率（费率单位为%） json 形式传入修改费率必须完整传入
    $qrcoderate = array(4, 4, 4, 4);
    $qrcodeList = [];
    for ($i = 0; $i < count($qrcodeType); $i++) {
        $qrcodeList[] = [
            "rateType" => $qrcodeType[$i],
            "rate" => $qrcoderate[$i]
        ];
    }
    $income->qrcodeList = $qrcodeList;

    //21贷记卡费率，不传默认0.60  22借记卡费率，不传默认0.55  23借记卡手续费封顶值，不传默认25  若传入，则对应三个类型必须同时传入
    $bankrate = array(0.60, 0.55, 25);
    //刷卡费率（费率单位为%，封顶值单位为元） json格式传入
    $bankType = array("21", "22", "23");
    $bankCardRates = [];
    for ($i = 0; $i < count($bankType); $i++) {
        $bankCardRates[] = [
            "rate" => $bankrate[$i],
            "type" => $bankType[$i]
        ];
    }
    $income->bankCardRates = $bankCardRates;

    //结算类型 03 T1    04 D1  不传入系统会根据合作机构签约时的默认结算产品执行，如需更改请联系对应商务经理
    $income->settleType = '03';
    //支持的支付渠道 01支付宝 02微信 03银联二维码  多选，逗号间隔。 不传默认全部开通
    $income->supportPayChannels = ["01", "02"];
    //支持的交易类型 01 主扫 02 被扫 03 公众号/小程序/服务窗/银 联JS 04退货 多选，逗号间隔。 不传默认全部开通
    $income->supportTradeTypes = ["01", "02"];
    //线上产品类型 线上类型商户必传 01APP 02网站 03公众号
    $income->onlineType = '01';
    //APP名称/网站网址/公众号名称 线上类型商户必传
    $income->onlineName = 'XXAPP';
    //APP下载地址及账号信息
    $income->onlineTypeInfo = 'XXXXXXXXXX';
    //营业执照注册名称
    $income->cprRegNmCn = '澧县xxxxx责任公司';
    //营业执照注册号
    $income->registCode = '914xxxxxxRLX3';
    //是否三证合一 企业必传 00 是 01 否
    $income->licenseMatch = '00';
    //商户经营详细地址
    $income->cprRegAddr = '西商xxxxxx无校验';
    //商户经营地址省编码
    $income->regProvCd = '130000000000';
    //商户经营地址市编码
    $income->regCityCd = '130700000000';
    //商户经营地址区编码
    $income->regDistCd = '130728000000';
    //经营类目
    $income->mccCd = '5309';
    //客服电话
    $income->csTelNo = '13812345678';
    //法人/商户负责人
    $income->identityName = '郭xx';
    //法人/商户负责人证件类型 00 身份证 03 军人证 04 警察证 05 港澳居民往来内地通行证 06 台湾居民来往大陆通行证 07 护照 98 单位证件 99 其他
    $income->identityTyp = '00';
    //法人/商户负责人证件号
    $income->identityNo = '421000200408092222';
    //结算账户名
    $income->actNm = '澧县xxxxx责任公司';
    //结算账户类型 00 对公 01 对私
    $income->actTyp = '00';
    //结算人证件号
    $income->stmManIdNo = '4324xxxxxxX';
    //结算卡号
    $income->actNo = '127913030410802';
    //开户银行
    $income->depoBank = '102';
    //开户省份
    $income->depoProvCd = '11';
    //开户城市
    $income->depoCityCd = '1100';
    //开户支行联行行号
    $income->lbnkNo = '787521000004';
    //营业执照照片
    $income->licensePic = 'd053af7e4cb4461aab1e6d99cbe2a8ab';
    //法人/商户负责人身份证正面（人像面）
    $income->legalPersonidPositivePic = 'd053af7e4cb4461aab1e6d99cbe2a8ab';
    //法人/商户负责人身份证正面（国徽面）
    $income->legalPersonidOppositePic = 'd053af7e4cb4461aab1e6d99cbe2a8ab';
    //开户许可证 对公结算必传
    $income->openingAccountLicensePic = 'd053af7e4cb4461aab1e6d99cbe2a8ab';
    //门头照片
    $income->storePic = 'd053af7e4cb4461aab1e6d99cbe2a8ab';
    //真实商户内景图片
    $income->insideScenePic = 'd053af7e4cb4461aab1e6d99cbe2a8ab';

    $client = new AopClient($config);


    $res = $client->execute($income);

    var_dump($res);
    //["applicationId"]=> string(32) "071d48a67b294fee8e384444d5436159"
    // ["mno"]=> string(15) "399200529394890"
    // ["orgId"]=> string(8) "26680846"
    // ["reqId"]=> string(23) "5ed0da2a7e07b9.71335359"
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

