<?php
//认证申请撤销

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\RealNameBack;

try {
    $query = new RealNameBack();

    $query->mno = '399200529394890';
    //微信申请单编号
    $query->wxApplyNo = '';

    $client = new AopClient($config);

    $res = $client->execute($query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

