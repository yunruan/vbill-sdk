<?php
//商户入网电子协议签署状态查询

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\MerchantElecSignatureSelect;

try {
    $signature = new MerchantElecSignatureSelect();

    $signature->mno = '399200529063975';

    $client = new AopClient($config);

    $res = $client->execute($signature);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

