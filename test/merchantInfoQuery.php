<?php
//商户信息查询

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\MerchantInfoQuery;

try {
    $query = new MerchantInfoQuery();

    $query->mno = '399200529394890';

    $client = new AopClient($config);

    $res = $client->execute($query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

