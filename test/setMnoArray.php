<?php
//设置分账

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\SetMnoArray;

try {
    $set_mno = new SetMnoArray();
    $set_mno->mno = '399190910000387';
    //商户订单号
    $set_mno->mnoArray = '399190618057330,399190618057330';

    $client = new AopClient($config);


    $res = $client->execute($set_mno);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

