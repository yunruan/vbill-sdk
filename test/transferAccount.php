<?php
//转账接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\TransferAccount;

try {
    $transfer = new TransferAccount();
    $transfer->mno = '399190910000387';
    //服务费出资方 01:转出方出手续费 02:转入方出手续费
    $transfer->investor = '01';
    //商户订单号
    $transfer->orderNo = 'MT_TEST_' . date('YmdHis');
    //转账金额；单位元，保留两位小数
    $transfer->amount = '0.01';
    //备注
    $transfer->content = '备注';

    $client = new AopClient($config);


    $res = $client->execute($transfer);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

