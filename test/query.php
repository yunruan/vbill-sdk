<?php
//支付查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\TradeQuery;

try {
    $trade_query = new TradeQuery();
    $trade_query->mno = '399190618057330';
    //商户订单号
    $trade_query->ordNo = 'MT_TEST_' . date('YmdHis');

    $client = new AopClient($config);


    $res = $client->execute($trade_query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

