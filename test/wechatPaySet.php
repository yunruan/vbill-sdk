<?php
//微信子商户支付参数配置

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\MerchantWechatPaySet;

try {
    $wechat = new MerchantWechatPaySet();

    $wechat->mno = '399200529063975';
    //微信子商户号
    $wechat->subMchId = '312329173';
    //配置类型 01 支付 Appid 02 关注 Appid 03 jsapi 授权目录
    $wechat->type = '01';
    //支付 Appid 配置类型为 01，02 时必传
    if($wechat->type == '01' || $wechat == '02'){
        $wechat->subAppid = 'wx32f13a1dda2db15d';
    }
    //支付 Appid 类型 配置类型为 01 时必传
    if($wechat->type == '01'){
        $wechat->accountType = '00';
    }
    if($wechat == '02'){
        //推荐关注公众号Appid 配置类型为02时与推荐关注 小程序Appid二选一
        $wechat->subscribeAppid = 'wx32f13a1dda2db15d';
        //推荐关注小程序Appid 配置类型为02时与推荐关注 公众号Appid二选一
        $wechat->receiptAppid = 'wx32f13a1dda2db15d';
    }
    //jsapi 授权目录 配置类型为 03 时必传
    if($wechat->type == '03'){
        $wechat->jsapiPath = 'http://www.baidu.com/Pay/';
    }


    $client = new AopClient($config);


    $res = $client->execute($wechat);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

