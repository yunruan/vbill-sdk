<?php
//批量交易总览查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\TradeQueryTotal;

try {
    $trade_query = new TradeQueryTotal();
    $trade_query->mno = '399190910000387';
    //账单日期 yyyyMMdd 查询区间为当天----前30天
    $trade_query->billTime = date('Ymd');

    $client = new AopClient($config);


    $res = $client->execute($trade_query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

