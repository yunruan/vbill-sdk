<?php
//商户入驻修改

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\UpdateMerchantInfo;

try {
    $update = new UpdateMerchantInfo();

    //商编
    $update->mno = '399200529394890';
    //商户简称
    $update->mecDisNm = '签购单的的名称';
    //商户联系手机号
    $update->mblNo = '13512345678';
    //经营类型 01 线下 02 线上 03 非盈利类 04 缴费类 05 保险类 06 私立院校
    $update->operationalType = '01';
    //资质类型 01 自然人 02 个体户 03 企业 线上商户必须是 03 企业类型
    $update->haveLicenseNo = '03';
    //商户类型 00 普通单店商户（非连锁商户）01 连锁总 02 连锁分 03 1+n 总 04 1+n 分
    $update->mecTypeFlag = '00';
    //所属总店商户编号（“入驻图片驳回”时不可修改） 商户类型为02，04 时必填  商户类型是02连锁分店时，总店类型必须是01连锁总店。 商户类型是04 1+n分店时，总店类型必须是03 1+n 总店。
    $update->parentMno = '499200529394890';
    //分店是否独立结算（“入驻图片驳回”时不可修改） 00是 01否 不传默认00独立结算  商户类型是02（连锁分店）时必传此参数  其他情况传入无效，系统强制为00独立结算
    $update->independentModel = '00';

    //01 微信02 支付宝06 银联单笔小亍等亍 1000 07 银联单笔大亍 1000
    $qrcodeType = array("01", "02", "06", "07");
    //二维码费率（费率单位为%） json 形式传入修改费率必须完整传入
    $qrcoderate = array(4, 4, 4, 4);
    $qrcodeList = [];
    for ($i = 0; $i < count($qrcodeType); $i++) {
        $qrcodeList[] = [
            "rateType" => $qrcodeType[$i],
            "rate" => $qrcoderate[$i]
        ];
    }
    $update->qrcodeList = $qrcodeList;

    //21贷记卡费率，不传默认0.60  22借记卡费率，不传默认0.55  23借记卡手续费封顶值，不传默认25  若传入，则对应三个类型必须同时传入
    $bankrate = array(0.60, 0.55, 25);
    //刷卡费率（“入驻图片驳回”时不可修改）（费率单位为%，封顶值单位为元） json格式传入
    $bankType = array("21", "22", "23");
    $bankCardRates = [];
    for ($i = 0; $i < count($bankType); $i++) {
        $bankCardRates[] = [
            "rate" => $bankrate[$i],
            "type" => $bankType[$i]
        ];
    }
    $update->bankCardRates = $bankCardRates;

    //线上产品类型 线上类型商户必传 01APP 02网站 03公众号
    $update->onlineType = '01';
    //APP名称/网站网址/公众号名称 线上类型商户必传
    $update->onlineName = 'XXAPP';
    //APP下载地址及账号信息
    $update->onlineTypeInfo = 'XXXXXXXXXX';
    //营业执照注册名称
    $update->cprRegNmCn = '澧县科技责任公司';
    //营业执照注册号
    $update->registCode = '91412345677RLX3';
    //是否三证合一 企业必传 00 是 01 否
    $update->licenseMatch = '00';
    //商户经营详细地址
    $update->cprRegAddr = '西商xxxxxx无校验';
    //商户经营地址省编码
    $update->regProvCd = '130000000000';
    //商户经营地址市编码
    $update->regCityCd = '130700000000';
    //商户经营地址区编码
    $update->regDistCd = '130728000000';
    //经营类目
    $update->mccCd = '5309';
    //客服电话
    $update->csTelNo = '13812345678';
    //法人/商户负责人
    $update->identityName = '郭xx';
    //法人/商户负责人证件类型 00 身份证 03 军人证 04 警察证 05 港澳居民往来内地通行证 06 台湾居民来往大陆通行证 07 护照 98 单位证件 99 其他
    $update->identityTyp = '00';
    //法人/商户负责人证件号
    $update->identityNo = '421000200408092222';
    //结算账户名
    $update->actNm = '澧县科技责任公司';
    //结算账户类型 00 对公 01 对私
    $update->actTyp = '00';
    //结算人证件号
    $update->stmManIdNo = '4324xxxxxxX';
    //结算卡号
    $update->actNo = '127913030410802';
    //开户银行
    $update->depoBank = '102';
    //开户省份
    $update->depoProvCd = '11';
    //开户城市
    $update->depoCityCd = '1100';
    //开户支行联行行号
    $update->lbnkNo = '787521000004';
    //营业执照照片
    $update->licensePic = '45f82864be4c4b45b8b9d6ddf6da36b7';
    //法人/商户负责人身份证正面（人像面）
    $update->legalPersonidPositivePic = '45f82864be4c4b45b8b9d6ddf6da36b7';
    //法人/商户负责人身份证正面（国徽面）
    $update->legalPersonidOppositePic = '45f82864be4c4b45b8b9d6ddf6da36b7';
    //开户许可证 对公结算必传
    $update->openingAccountLicensePic = '45f82864be4c4b45b8b9d6ddf6da36b7';
    //门头照片
    $update->storePic = '45f82864be4c4b45b8b9d6ddf6da36b7';
    //真实商户内景图片
    $update->insideScenePic = '45f82864be4c4b45b8b9d6ddf6da36b7';

    $client = new AopClient($config);


    $res = $client->execute($update);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

