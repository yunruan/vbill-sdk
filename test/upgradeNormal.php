<?php
//商户类型变更接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\UpgradeNormal;

try {
    $upgrade_normal = new UpgradeNormal();
    $upgrade_normal->mno = '399190910000387';

    //变更类型 00普通商户转连锁总 01普通商户转1+n总
    $upgrade_normal->changeType = '00';

    $client = new AopClient($config);

    $res = $client->execute($upgrade_normal);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

