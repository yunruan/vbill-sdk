<?php
//单页交易流水查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\TradeQueryPage;

try {
    $trade_query = new TradeQueryPage();
    $trade_query->mno = '399190910000387';
    //账单日期 yyyyMMdd 查询区间为当天----前30天
    $trade_query->billTime = date('Ymd');
    //页数
    $trade_query->page = 5;

    $client = new AopClient($config);


    $res = $client->execute($trade_query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

