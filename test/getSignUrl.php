<?php
//分账协议签署接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\SignUrl;

try {
    $sign = new SignUrl();

    $sign->mno = '399290754110000';
    //签约类型 00：接口签约01：短信签约
    $sign->signType = '00';

    $client = new AopClient($config);


    $res = $client->execute($sign);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

