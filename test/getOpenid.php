<?php
//微信获取 openid 接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\SubOpenid;

try {
    $sub_openid = new SubOpenid();
    $sub_openid->mno = '399190618057330';
    $sub_openid->subMchId = 'wx24210004370ec43b';
    $sub_openid->subAppId = 'subAppid';
    $sub_openid->authCode = 'authCode';

    $client = new AopClient($config);


    $res = $client->execute($sub_openid);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

