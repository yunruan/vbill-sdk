<?php
//入驻结果查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\QueryMerchantInfo;

try {
    $query = new QueryMerchantInfo();

    $query->mno = '399200529394890';
    $query->applicationId = 'fc6c2db77d0b4a45a8c4f726c8acb259';

    $client = new AopClient($config);

    $res = $client->execute($query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

