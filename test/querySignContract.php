<?php
//分账协议签署查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\SignQuery;

try {
    $query = new SignQuery();
    //修改申请 ID
    $query->mno = '399290754110000';

    $client = new AopClient($config);

    $res = $client->execute($query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

