<?php
//聚合支付接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\JSAPI;

try {
    $jsapi = new JSAPI();
    //商户订单号,64个字符以内、只能包含字母、数字、下划线；需保证在商户端不重复
    $jsapi->ordNo = 'MT_TEST_' . date('YmdHis');
    //商户入驻返回的商户编号
    $jsapi->mno = '399190618057330';
    //订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
    $jsapi->amt = '0.01';
    //支付渠道 WECHAT: 微信 ALIPAY:支付宝 UNIONPAY:银联
    $jsapi->payType = 'WECHAT';
    //支付方式 02 微信公众号/支付宝服务窗/银联js支付/支付宝小程序 03 微信小程序
    $jsapi->payWay = '02';
    //订单标题
    $jsapi->subject = '聚合支付测试';
    //交易来源 01：服务商 02：收银台 03：硬件
    $jsapi->tradeSource = '01';
    //商家ip地址
    $jsapi->trmIp = '127.0.0.1';
    //用户号 微信：openid 支付宝：userid 银联：userid 微信&支付宝必传 银联js为非必传
    $jsapi->userId = '2088101117955611';
    //支付结果通知地址
    $jsapi->notifyUrl = 'https://example.com';

    $client = new AopClient($config);


    $res = $client->execute($jsapi);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

