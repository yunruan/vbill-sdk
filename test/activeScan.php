<?php
//主扫接口 用户通过扫描商家提供的收款码进行付款

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\ActiveScan;

try {
    $active_scan = new ActiveScan();
    //商户编号
    $active_scan->mno = '399190618057330';
    //商户订单号
    $active_scan->ordNo = 'MT_TEST_' . date('YmdHis');
    //订单总金额 单位元，精确到小数点后两位
    $active_scan->amt = '0.01';
    //支付渠道 取值范围：WECHAT:微信, ALIPAY:支付宝, UNIONPAY: 银联
    $active_scan->payType = 'WECHAT';
    //订单标题
    $active_scan->subject = '主扫支付测试';
    //交易来源；01：服务商02：收银台03：硬件
    $active_scan->tradeSource = '01';
    //商家ip地址
    $active_scan->trmIp = '127.0.0.1';
    //支付结果通知地址
    $active_scan->notifyUrl = 'https://example.com';

    $client = new AopClient($config);

    $res = $client->execute($active_scan);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

