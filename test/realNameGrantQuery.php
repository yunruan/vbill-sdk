<?php
//微信子商户授权状态查询

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\RealNameGrantQuery;

try {
    $query = new RealNameGrantQuery();

    //微信子商户号
    $query->subMchId = '399200529394890';

    $client = new AopClient($config);

    $res = $client->execute($query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

