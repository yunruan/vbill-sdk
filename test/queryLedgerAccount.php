<?php
//分账查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\QueryLedgerAccount;

try {
    $ledger_query = new QueryLedgerAccount();
    $ledger_query->mno = '399190910000387';
    //商户订单号
    $ledger_query->ordNo = 'MT_TEST_' . date('YmdHis');

    $client = new AopClient($config);


    $res = $client->execute($ledger_query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

