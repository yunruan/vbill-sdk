<?php
//转账查询接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\TransferAccountQuery;

try {
    $transfer = new TransferAccountQuery();
    //商户订单号
    $transfer->orderNo = 'MT_TEST_' . date('YmdHis');

    $client = new AopClient($config);

    $res = $client->execute($transfer);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

