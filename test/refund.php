<?php
//申请退款接口

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\Refund;

try {
    $refund = new Refund();
    //商户退款订单号
    $refund->ordNo = 'MT_TEST_' . date('YmdHis');
    //商户入驻返回的商户编号
    $refund->mno = '399190618057330';
    //商户订单号
    $refund->origOrderNo = '2088101117955611';
    //退款金额
    $refund->amt = '0.02';
    //退款结果通知地址
    $refund->notifyUrl = 'http://example.com';


    $client = new AopClient($config);


    $res = $client->execute($refund);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

