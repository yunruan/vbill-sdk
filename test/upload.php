<?php
//图片上传

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\Upload;

try {
    $upload = new Upload();
    $upload->pictureType = '01';
    $upload->file = fopen('D:/wallpaper/26.jpg','r');

    $client = new AopClient($config);


    $res = $client->upload($upload);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

