<?php
//微信子商户支付参数查询

require '../vendor/autoload.php';
$config = require_once './config.php';

use Vbill\AopClient;
use Vbill\Request\MerchantWechatPaySetQuery;

try {
    $query = new MerchantWechatPaySetQuery();

    $query->mno = '399200529063975';
    //微信子商户号
    $query->subMchId = '312329173';
    $client = new AopClient($config);

    $res = $client->execute($query);

    var_dump($res);
} catch (\Vbill\Exception\ApiBusinessException $e){
    echo $e->getMessage() . '('.$e->getCode().')';
} catch (\Throwable $th) {
    var_dump((string) $th);
}

