<?php


namespace Vbill\Exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ApiResponseException extends Exception
{
    protected $request;
    protected $response;

    public function __construct(String $message = '', int $code = 0,  ?RequestInterface $request = null, ?ResponseInterface $response = null, Throwable $previous = NULL)
    {
        $this->setRequest($request);
        $this->setResponse($response);

        parent::__construct($message, $code, $previous);
    }

    public function setRequest(?RequestInterface $request = null)
    {
        $this->request = $request;
    }

    public function getRequest(): ?RequestInterface
    {
        return $this->request;
    }

    public function setResponse(?ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }
}
