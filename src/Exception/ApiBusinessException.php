<?php


namespace Vbill\Exception;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ApiBusinessException extends Exception
{
    protected $request;
    protected $response;

    public function __construct(String $message = '', int $code = 0, ?ResponseInterface $response = null,  ?RequestInterface $request = null, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }

    public function setRequest(?RequestInterface $request = null)
    {
        $this->request = $request;
    }

    public function getRequest(): ?RequestInterface
    {
        return $this->request;
    }

    public function setResponse(?ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }
}