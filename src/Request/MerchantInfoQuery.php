<?php
namespace Vbill\Request;

class MerchantInfoQuery extends Request
{
    protected $uri = 'merchant/merchantInfoQuery';
}
