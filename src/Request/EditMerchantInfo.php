<?php
namespace Vbill\Request;

class EditMerchantInfo extends Request
{
    protected $uri = 'merchant/editMerchantInfo';
}
