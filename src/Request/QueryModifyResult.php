<?php
namespace Vbill\Request;

class QueryModifyResult extends Request
{
    protected $uri = 'merchant/queryModifyResult';
}
