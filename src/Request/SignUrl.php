<?php
namespace Vbill\Request;

class SignUrl extends Request
{
    protected $uri = 'merchant/sign/getUrl';
}
