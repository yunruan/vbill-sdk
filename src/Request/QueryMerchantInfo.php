<?php
namespace Vbill\Request;

class QueryMerchantInfo extends Request
{
    protected $uri = 'merchant/queryMerchantInfo';
}
