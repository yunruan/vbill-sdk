<?php
namespace Vbill\Request;

class Refund extends Request
{
    protected $uri = 'order/refund';
}
