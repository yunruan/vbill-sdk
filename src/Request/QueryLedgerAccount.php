<?php
namespace Vbill\Request;

class QueryLedgerAccount extends Request
{
    protected $uri = 'query/ledger/queryLedgerAccount';
}
