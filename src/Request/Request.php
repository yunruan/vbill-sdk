<?php
namespace Vbill\Request;

class Request
{
    // 接口方法名
    protected $uri = '';

    // 接口方法
    protected $method = 'POST';

    // 签名类型
    protected $signType = 'RSA';

    // 业务参数
    protected $data = [];

    public function __construct(array $data = [])
    {
        $this->setData($data);
    }

    /**
     * 获取接口签名方式
     *
     * @return string
     */
    public function getSignType(): string
    {
        return $this->signType;
    }

    /**
     * 获取请求方式
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * 获取接口地址
     *
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * 获取业务参数
     *
     * @param String $name              参数名称
     * @return array|string|null
     */
    public function getData(String $name = null)
    {
        if(is_null($name)){
            return $this->data;
        }else{
            return $this->data[$name]??null;
        }

    }

    /**
     * 设置业务参数
     *
     * @param string|array $name        参数名
     * @param $value        参数值
     * @return void
     */
    public function setData($name, $value = null)
    {
        if(is_string($name)){
            $this->data[$name] = $value;
        }else if(is_array($name)){
            $this->data = $name;
        }
    }

    /**
     * 判断业务参数是否存在
     *
     * @param string $name          参数名
     * @return boolean
     */
    public function hasData(String $name): bool
    {
        // 将字段名拆分成数组
        $nameArr =  explode('.', $name);
        // 数据
        $data = $this->data;

        foreach ($nameArr as $nameStr) {
            if (!isset($data[$nameStr])) {
                return false;
            }

            $data = $data[$nameStr];
        }

        return true;
    }

    /**
     * 设置参数
     *
     * @param String $name      参数名
     * @param Mixed $value     参数值
     * @return void
     */
    public function __set(String $name, $value): void
    {
        $this->setData($name, $value);
    }

    /**
     * 获取参数
     *
     * @param String $name      参数名
     * @return mixed
     */
    public function __get(String $name)
    {
        return $this->getData($name);
    }

    /**
     * 判断参数是否存在
     *
     * @param String $name      参数名
     * @return boolean
     */
    public function __isset(String $name): bool
    {
        return $this->hasData($name);
    }

}
