<?php
namespace Vbill\Request;

class RealNameGrantQuery extends Request
{
    protected $uri = 'merchant/realName/queryGrantStatus';
}
