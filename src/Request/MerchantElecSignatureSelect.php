<?php
namespace Vbill\Request;

class MerchantElecSignatureSelect extends Request
{
    protected $uri = 'merchant/elecSignature/selectSign';
}
