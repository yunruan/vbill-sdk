<?php
namespace Vbill\Request;

class UpdateMerchantInfo extends Request
{
    protected $uri = 'merchant/updateMerchantInfo';
}
