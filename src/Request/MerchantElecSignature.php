<?php
namespace Vbill\Request;

class MerchantElecSignature extends Request
{
    protected $uri = 'merchant/elecSignature/openElecSignature';
}
