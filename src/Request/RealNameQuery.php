<?php
namespace Vbill\Request;

class RealNameQuery extends Request
{
    protected $uri = 'merchant/realName/queryApplyInfo';
}
