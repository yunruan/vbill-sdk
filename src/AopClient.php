<?php
namespace Vbill;

use Vbill\Exception\Exception;
use Vbill\Exception\ApiBusinessException;
use Vbill\Exception\ApiResponseException;
use Vbill\Request\Request;
use Vbill\MerchantRequest\Request as MerchantRequest;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class AopClient extends Base
{
    // 版本
    const VERSION = '1.0';

    // 接口域名
    const API_DOMAIN = [
        'prod' => 'https://openapi.tianquetech.com',        // 生产环境
        'test' => 'https://openapi-test.tianquetech.com'    // 测试环境
    ];

    // 环境
    protected $env = 'prod';

    // 合作结构id
    protected $orgId;

    // 公钥
    protected $publicKey;

    // 私钥
    protected $privateKey;

    protected $httpClient=null;

    public function __construct(Array $config = [])
    {
        $this->setConfig($config);
    }

    /**
     * 执行请求。
     *
     * @param Request $request      请求类
     * @return void
     */
    public function execute(Request $request ): array
    {
        $params = [
            "orgId" => $this->getConfig('orgId'),
            "reqId" => $this->buildRquestId(),
            "signType" => $request->getSignType(),
            'reqData' =>  $request->getData(),
            "timestamp" => date("Y-m-d h:i:s"),
            "version" => self::VERSION,
        ];

        $params['sign'] = $this->buildSign($params);

        $method = $request->getMethod();

        $url = $this->buildApiUrl($request->getUri());

        $response = $this->request($url, $method, json_encode($params, 320),[], []);

        // 响应字符串
        $resStr = (String) $response->getBody();

        // 响应数据
        $resArr = json_decode($resStr, true);
        if($resArr === false){
            throw new Exception("接口返回数据解析失败");
        }

        if(!isset($resArr['code']) || $resArr['code'] != '0000'){
            $code =  intval($resArr['code']);
            $message = $resArr['msg'] ?? '接口返回异常';
            throw new ApiBusinessException($message, $code, $response);
        }

        // 验签
        $waitVerifyData = $resArr;
        $waitVerifySign = $waitVerifyData['sign'];
        unset($waitVerifyData['sign']);
        if(!$this->verifySign($waitVerifyData, $waitVerifySign)){
            throw new Exception("同步签名验证失败");
        }

        return $resArr;
    }

    /**
     * 构建api 请求地址
     *
     * @param String $uri       接口名
     * @param String $env       环境
     * @return string
     */
    protected function buildApiUrl(String $uri, String $env = null): string
    {
        $uri = trim($uri, '\\/');

        if($env == null){
            $env = $this->getConfig('env');
        }

        $domain = trim(self::API_DOMAIN[$env], '\\/');

        $url = $domain . '/' . $uri;

        return $url;
    }

    /**
     * 构建签名
     *
     * @param Array $params     待签名参数
     * @return string
     */
    public function buildSign(Array $params): string
    {
        $rsaPrivateKey = $this->privateKey;
        $signType = $params['signType'] ?? 'RSA';

        $stringToBeSigned = $this->buildSignDataStr($params);

        $privateKey = $this->formatSecretKey($rsaPrivateKey, 'pri');

        if ("RSA2" == $signType) {
            openssl_sign($stringToBeSigned, $sign, $privateKey, OPENSSL_ALGO_SHA256);
        } else {
            openssl_sign($stringToBeSigned, $sign, $privateKey);
        }

        $sign = base64_encode($sign);

        return $sign;
    }

    /**
     * 验证签名
     *
     * @param Array $params     验证数据
     * @param String $sign      签名内容
     * @return boolean
     */
    public function verifySign(Array $params, String $sign): bool
    {

        $stringToBeSigned = $this->buildSignDataStr($params);

        $pubKeyPem = $this->formatSecretKey($this->publicKey, 'pub');
        //转换为openssl密钥，必须是没有经过pkcs8转换的公钥
        $res = openssl_get_publickey($pubKeyPem);
        //base64解码签名
        $signBase64 = base64_decode($sign);
        //调用openssl内置方法验签，返回bool值
        $result = (bool) openssl_verify($stringToBeSigned, $signBase64, $res);
        //释放资源
        openssl_free_key($res);
        //返回资源是否成功
        return $result;
    }

    /**
     * 构建签名数据字符串
     *
     * @param Array $params
     * @return string
     */
    public function buildSignDataStr(Array $params): string
    {
        ksort($params);
        $stringToBeSigned = "";
        foreach ($params as $k => $v) {
            $isarray = is_array($v);
            if ($isarray) {
                $stringToBeSigned .= "$k" . "=" . json_encode($v, 320) . "&";
            } else {
                $stringToBeSigned .= "$k" . "=" . "$v" . "&";
            }
        }
        unset($k, $v);
        $stringToBeSigned = substr($stringToBeSigned, 0, strlen($stringToBeSigned) - 1);

        return $stringToBeSigned;
    }

    /**
     * 请求接口
     *
     * @param String $url           地址
     * @param String $method        请求方式
     * @param String $body          请求body
     * @param array $query          请求query
     * @param array $headers        请求header
     * @return ResponseInterface
     */
    protected function request(String $url, String $method,  String $body = '', array $query = [], array $headers = []): ResponseInterface
    {
        try {
            $headers = array_merge([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ], $headers);

            $client = $this->getHttpClient();

            $response = $client->request($method, $url, [
                'headers' => $headers,
                'body' => $body,
                'query' => $query,
                'expect' => false,
            ]);

            return $response;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            // 接口返回信息
            $responseBody = (string) $e->getResponse()->getBody();
            $message = $responseBody?:'请求接口失败';

            // 接口返回码
            $code = $e->getResponse()->getStatusCode();

            throw new ApiResponseException($message, $code, $e->getRequest(), $e->getResponse(), $e);
        }
    }

    /**
     * 执行上传请求。
     *
     * @param Request $request      请求类
     * @return void
     */
    public function upload(Request $request): array
    {
        $params = [
            "orgId" => $this->getConfig('orgId'),
            "reqId" => $this->buildRquestId(),
        ];
        $data = $request->getData();
        $params = array_merge($params,$data);

        $method = $request->getMethod();

        $url = $this->buildApiUrl($request->getUri());

        $response = $this->upload_request($url, $method, $params,[], []);

        // 响应字符串
        $resStr = (String) $response->getBody();

        // 响应数据
        $resArr = json_decode($resStr, true);
        if($resArr === false){
            throw new Exception("接口返回数据解析失败");
        }

        if(!isset($resArr['code']) || $resArr['code'] != '0000'){
            $code =  intval($resArr['code']);
            $message = $resArr['msg'] ?? '接口返回异常';
            throw new ApiBusinessException($message, $code, $response);
        }

        return $resArr;
    }
    /**
     * 请求上传接口
     *
     * @param String $url           地址
     * @param String $method        请求方式
     * @param String $body          请求body
     * @param array $query          请求query
     * @param array $headers        请求header
     * @return ResponseInterface
     */
    protected function upload_request(String $url, String $method,  Array $body = []): ResponseInterface
    {
        try {
            $client = $this->getHttpClient();

            $data = [];
            foreach ($body as $k => $v){
                $data[] = ['name' => $k , 'contents' => $v];
            }

            $response = $client->request($method, $url, [
                'multipart' => $data
            ]);

            return $response;

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            // 接口返回信息
            $responseBody = (string) $e->getResponse()->getBody();
            $message = $responseBody?:'请求接口失败';

            // 接口返回码
            $code = $e->getResponse()->getStatusCode();

            throw new ApiResponseException($message, $code, $e->getRequest(), $e->getResponse(), $e);
        }
    }


    /**
     * 创建请求id
     *
     * @return string
     */
    protected function buildRquestId(): string
    {
        return uniqid('', true);
    }

    /**
     * 获取配置参数
     *
     * @param string $name         配置名称
     * @return mixed               配置值
     */
    public function getConfig(string $name)
    {
        return $this->$name ?? null;
    }

    /**
     * 设置配置参数
     *
     * @param string|array $name             若为string则代表配置参数名。 若为数组则是整个参数
     * @param mixed $value                   当$name为string类型时, 参数的值
     * @return void
     */
    public function setConfig($name, ?String $value = null)
    {
        if (is_string($name)) {
            $config = [$name => $value];
        }else{
            $config = $name;
        }

        foreach ($config as $name => $value) {
            if(property_exists($this, $name)){
                $this->$name = $value;
            }
        }
    }

    /**
     * 格式化秘钥
     *
     * @param string $secret_key
     * @param string $type
     * @return string
     */
    protected function formatSecretKey(string $secret_key, string $type): string
    {
        // 64个英文字符后接换行符"\n",最后再接换行符"\n"
        $key = (wordwrap($secret_key, 64, "\n", true)) . "\n";
        // 添加pem格式头和尾
        if ($type == 'pub') {
            $pem_key = "-----BEGIN PUBLIC KEY-----\n" . $key . "-----END PUBLIC KEY-----\n";
        } else if ($type == 'pri') {
            $pem_key = "-----BEGIN RSA PRIVATE KEY-----\n" . $key . "-----END RSA PRIVATE KEY-----\n";
        } else {
            throw new Exception("公私钥类型非法", 1);

        }
        return $pem_key;
    }

    /**
     * 获取 http 客户端
     *
     * @return ClientInterface
     */
    public function getHttpClient(): ClientInterface
    {

        if (!($this->httpClient instanceof ClientInterface)) {
            $this->httpClient = new Client();
        }
 
        return $this->httpClient;
    }

    /**
     * 设置http 客户端
     *
     * @param ClientInterface $client
     * @return void
     */
    public function setHttpClient(ClientInterface $client)
    {
        $this->httpClient = $client;
    }

}
