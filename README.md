# vbill-sdk

#### 介绍
随行付sdk

#### 软件架构
````
|-- src                                       --核心目录
|   |-- AopClient.php                         --核心类
|   |-- Base.php                              --基类
|   |-- Exception                             --异常
|   |   |-- ApiBusinessException.php          --业务异常
|   |   |-- ApiResponseException.php          --请求异常
|   |   |-- Exception.php                     --异常基类
|   |-- MerchantRequest                       --商户请求
|   |   |-- EditMerchantInfo.php              --商户信息修改接口        
|   |   |-- Income.php                        --商户入驻
|   |   |-- MerchantElecSignature.php         --商户入网电子协议签署
|   |   |-- MerchantElecSignatureSelect.php   --商户入网电子协议签署状态查询
|   |   |-- MerchantInfoQuery.php             --商户信息查询
|   |   |-- MerchantSetup.php                 --商户设置
|   |   |-- MerchantWechatPaySet.php          --微信子商户支付参数配置
|   |   |-- MerchantWechatPaySetQuery.php     --微信子商户支付参数查询
|   |   |-- QueryMerchantInfo.php             --入驻结果查询接口
|   |   |-- QueryModifyResult.php             --商户修改结果查询
|   |   |-- Request.php                       --接口基类
|   |   |-- SignQuery.php                     --分账协议签署查询接口
|   |   |-- SignUrl.php                       --分账协议签署接口
|   |   |-- UpdateMerchantInfo.php            --商户入驻修改
|   |   |-- UpgradeMerchantInfo.php           --小微商户补充资料升级接口
|   |   |-- Upload.php                        --图片上传
|   |-- Request                               --交易请求
|   |   |-- Close.php                         --关单接口
|   |   |-- JSAPI.php                         --聚合支付接口
|   |   |-- LaunchLedger.php                  --分账接口
|   |   |-- PoundageQuery.php                 --手续费查询接口
|   |   |-- QueryLedgerAccount.php            --分账查询接口
|   |   |-- Refund.php                        --申请退款接口
|   |   |-- RefundQuery.php                   --退款查询接口
|   |   |-- Request.php                       --接口基类
|   |   |-- SetMnoArray.php                   --设置分账
|   |   |-- SubOpenid.php                     --微信获取 openid 接口
|   |   |-- TradeQueryPage.php                --单页交易流水查询接口
|   |   |-- TradeQuery.php                    --支付查询接口
|   |   |-- TradeQueryTotal.php               --批量交易总览查询接口
|   |   |-- TransferAccount.php               --转账接口
|   |   |-- TransferAccountQuery.php          --转账查询接口
|-- test                                      --支付交易相关示例demo
|   |-- close.php
|   |-- config.php
|   |-- getOpenid.php
|   |-- jsapi.php
|   |-- launchLedger.php
|   |-- poundageQuery.php
|   |-- queryLedgerAccount.php
|   |-- queryPage.php
|   |-- query.php
|   |-- queryTotal.php
|   |-- refund.php
|   |-- refundQuery.php
|   |-- setMnoArray.php
|   |-- transferAccount.php
|   |-- transferAccountQuery.php
|-- merchant                                  --商户进件相关示例demo
|   |-- config.php
|   |-- editMerchantInfo.php
|   |-- elecsignature.php
|   |-- elecsignatureSelect.php
|   |-- getSignUrl.php
|   |-- income.php
|   |-- merchantInfoQuery.php
|   |-- merchantSetup.php
|   |-- queryMerchantInfo.php
|   |-- queryModifyResult.php
|   |-- querySignContract.php
|   |-- updateMerchantInfo.php
|   |-- upgradeMerchantInfo.php
|   |-- upload.php
|   |-- wechatPaySet.php
|   |-- wechatPaySetQuery.php
|-- composer.json
|-- composer.lock
|-- README.en.md
|-- README.md
````


#### 安装教程
````
composer install
````

#### 使用说明
    * 商户进件相关示例参考merchant目录下demo示例
    * 商户交易相关示例参考test目录下demo示例
